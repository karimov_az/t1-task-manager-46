package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.karimov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.karimov.tm.repository.dto.TaskDtoRepository;
import ru.t1.karimov.tm.repository.dto.UserDtoRepository;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;
import ru.t1.karimov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int HALF_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES / 2;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final List<TaskDto> taskList = new ArrayList<>();

    @NotNull
    private static final List<ProjectDto> projectList = new ArrayList<>();

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String PROJECT1_ID = "";

    @NotNull
    private static String PROJECT2_ID = "";

    @Nullable
    private static ITaskDtoRepository taskRepository;

    @Nullable
    private static IProjectDtoRepository projectRepository;

    @Nullable
    private static IUserDtoRepository userRepository;

    @Nullable
    private static EntityManager entityManager;

    @BeforeClass
    public static void createUsers() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @NotNull final UserDto user1 = new UserDto();
        user1.setLogin("test1");
        user1.setPasswordHash(HashUtil.salt(propertyService, "test1"));
        entityManager.getTransaction().begin();
        userRepository.add(user1);
        entityManager.getTransaction().commit();
        USER1_ID = user1.getId();

        @NotNull final UserDto user2 = new UserDto();
        user2.setLogin("test2");
        user2.setPasswordHash(HashUtil.salt(propertyService, "test2"));
        entityManager.getTransaction().begin();
        userRepository.add(user2);
        entityManager.getTransaction().commit();
        USER2_ID = user2.getId();

        entityManager.close();
    }

    @Before
    public void initRepository() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);
        taskRepository = new TaskDtoRepository(entityManager);

        for (int i = 0; i < 2; i++) {
            @NotNull final ProjectDto project = new ProjectDto();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i == 0) {
                project.setUserId(USER1_ID);
                PROJECT1_ID = project.getId();
            }
            else {
                project.setUserId(USER2_ID);
                PROJECT2_ID = project.getId();
            }
            entityManager.getTransaction().begin();
            projectList.add(project);
            projectRepository.add(project);
            entityManager.getTransaction().commit();

            for (int j = 0; j < HALF_NUMBER_OF_ENTRIES; j++) {
                @NotNull final TaskDto task = new TaskDto();
                task.setName("Task " + j);
                task.setDescription("Description " + j);
                if (j == 1 || j == 3 ) task.setStatus(Status.IN_PROGRESS);
                if (i == 0) {
                    task.setUserId(USER1_ID);
                    task.setProjectId(PROJECT1_ID);
                }
                else {
                    task.setUserId(USER2_ID);
                    task.setProjectId(PROJECT2_ID);
                }
                entityManager.getTransaction().begin();
                taskRepository.add(task);
                taskList.add(task);
                entityManager.getTransaction().commit();
            }
        }

        entityManager.close();
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        entityManager.getTransaction().begin();
        userRepository.removeOneById(USER1_ID);
        userRepository.removeOneById(USER2_ID);
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @After
    public void initClear() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);
        taskRepository = new TaskDtoRepository(entityManager);

        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            entityManager.getTransaction().begin();
            taskRepository.removeOneById(userId, task.getId());
            entityManager.getTransaction().commit();
        }
        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            entityManager.getTransaction().begin();
            projectRepository.removeOneById(userId, project.getId());
            entityManager.getTransaction().commit();
        }
        taskList.clear();
        projectList.clear();

        entityManager.close();
    }

    @Test
    public void testAdd() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final TaskDto task = new TaskDto();
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = task.getId();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(USER1_ID);
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        @Nullable final TaskDto actualTask = taskRepository.findOneById(USER1_ID, id);
        assertNotNull(actualTask);
        assertEquals(USER1_ID, actualTask.getUserId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());

        @Nullable final String userId = actualTask.getUserId();
        assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(userId, actualTask.getId());
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final List<TaskDto> emptyList = new ArrayList<>();
        entityManager.getTransaction().begin();
        taskRepository.removeAll(USER1_ID);
        entityManager.getTransaction().commit();
        assertEquals(emptyList, taskRepository.findAll(USER1_ID));
        assertNotEquals(emptyList, taskRepository.findAll(USER2_ID));

        entityManager.close();
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        entityManager.getTransaction().begin();
        taskRepository.removeAll("Other_id");
        entityManager.getTransaction().commit();
        final int numberAllTasks = taskRepository.getSize(USER1_ID).intValue() + taskRepository.getSize(USER2_ID).intValue();
        assertEquals(NUMBER_OF_ENTRIES, numberAllTasks);

        entityManager.close();
    }

    @Test
    public void testFindAllByUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final List<TaskDto> expectedTasks = taskList.stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<TaskDto> actualTasks = taskRepository.findAll(USER1_ID);
        assertEquals(expectedTasks.size(), actualTasks.size());

        entityManager.close();
    }

    @Test
    public void testFindAllSortByUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        for (@NotNull final TaskSort sort : TaskSort.values()) {
            @NotNull final List<TaskDto> actualTaskList = taskRepository.findAll(USER1_ID, sort.getComparator());
            @NotNull final List<TaskDto> expectedTaskList = taskList.stream()
                    .filter(m -> USER1_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            assertEquals(expectedTaskList.size(), actualTaskList.size());
        }

        entityManager.close();
    }

    @Test
    public void testFindOneByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            @Nullable final TaskDto actualTask = taskRepository.findOneById(userId, id);
            assertNotNull(actualTask);
            assertEquals(id, actualTask.getId());
        }

        entityManager.close();
    }

    @Test
    public void testFindOneByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(taskRepository.findOneById(USER1_ID, UUID.randomUUID().toString()));
        }

        entityManager.close();
    }

    @Test
    public void testFindAllByProjectIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            @NotNull final String projectId = userId.equals(USER1_ID) ? PROJECT1_ID : PROJECT2_ID;
            @NotNull final List<TaskDto> expectedList = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .filter(m -> projectId.equals(m.getProjectId()))
                    .collect(Collectors.toList());
            @NotNull final List<TaskDto> actualList = taskRepository.findAllByProjectId(userId, projectId);
            assertEquals(expectedList.size(), actualList.size());
        }

        entityManager.close();
    }

    @Test
    public void testFindAllByProjectIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            @NotNull final String projectId = userId.equals(USER1_ID) ? PROJECT2_ID : PROJECT1_ID;
            @NotNull final List<TaskDto> actualList = taskRepository.findAllByProjectId(userId, projectId);
            assertEquals(0, actualList.size());
        }

        entityManager.close();
    }

    @Test
    public void testExistByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            assertTrue(taskRepository.existsById(userId, id));
        }

        entityManager.close();
    }

    @Test
    public void testExistByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = UUID.randomUUID().toString();
            assertFalse(taskRepository.existsById(userId, id));
        }

        entityManager.close();
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            final int taskRepositorySize = taskRepository.getSize(userId).intValue();
            @Nullable final List<TaskDto> actualList = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(actualList.size(), taskRepositorySize);
        }

        entityManager.close();
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            assertNotNull(userId);
            entityManager.getTransaction().begin();
            taskRepository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
            assertNull(taskRepository.findOneById(userId, id));
        }

        entityManager.close();
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        taskRepository = new TaskDtoRepository(entityManager);

        final int expectedSize = taskRepository.getSize().intValue();
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(USER1_ID, otherId);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(USER2_ID, otherId);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(otherUserId,otherId);
        entityManager.getTransaction().commit();
        final int actualSize = taskRepository.getSize().intValue();
        assertEquals(expectedSize, actualSize);

        entityManager.close();
    }

}
