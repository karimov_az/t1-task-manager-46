package ru.t1.karimov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.model.IProjectRepository;
import ru.t1.karimov.tm.model.Project;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT count(p) FROM Project p";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final String jpql = "SELECT p FROM Project p";
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final Comparator<Project> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT p FROM Project p ORDER BY p." + sort;
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT p FROM Project p";
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT p FROM Project p WHERE p.user.id = :userId";
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class)
                .setParameter(USER_ID, userId);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator
    ) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p." + sort;
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class)
                .setParameter(USER_ID, userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT p FROM Project p WHERE p.id = :id AND p.user.id = :userId";
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT p FROM Project p WHERE p.user.id = :userId";
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class)
                .setParameter(USER_ID, userId)
                .setFirstResult(index);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull final String userId) throws Exception {
        @NotNull String jpql = "SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class)
                .setParameter(USER_ID, userId);
        return query.getSingleResult();
    }

}
