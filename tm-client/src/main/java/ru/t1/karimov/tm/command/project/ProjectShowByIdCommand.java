package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.karimov.tm.dto.response.project.ProjectGetByIdResponse;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    public static final String NAME = "project-show-by-id";

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final ProjectGetByIdResponse response = getProjectEndpoint().getProjectById(request);
        @Nullable final ProjectDto project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
